#ifndef  __CONFIG__
#define  __CONFIG__

#include "stdint.h"

#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80

#define RED_LED     BIT1
#define BLUE_LED    BIT2
#define GREEN_LED   BIT3

#define RCGCGPIO    (*((volatile unsigned long *)0x400FE608)) // clock gating register for GPIO
#define GPIODEN_PORT_F     (*((volatile unsigned long *)0x4002551C)) // Digital enable register for port F
#define GPIODIR_PORT_F     (*((volatile unsigned long *)0x40025400)) // GPIO direction register for Port F
#define GPIODATA_PORT_F     (*((volatile unsigned long *)0x400253FC)) // GPIO Data register for Port F
#define GPIOPUR_PORT_F     (*((volatile unsigned long *)0x40025510))  // GPIO pull up resistor select register for Port F

/* The GPIOLOCK register enables write access to the GPIOCR register (see page 685). Writing
0x4C4F.434B to the GPIOLOCK register unlocks the GPIOCR register. */
#define GPIOLOCK_PORT_F (*((volatile unsigned long *)0x40025520))  // GPIOLOCK register for port F

/*
0 - The corresponding GPIOAFSEL, GPIOPUR, GPIOPDR, or
GPIODEN bits cannot be written.

1 - The corresponding GPIOAFSEL, GPIOPUR, GPIOPDR, or
GPIODEN bits can be written. */
#define GPIOCR_PORT_F  (*((volatile unsigned long *)0x40025524))   // GPIO commit register for port F

/*GPIO Interrupt Sense GPIOIS 0 - edge, 1 - level*/
#define GPIOIS_PORT_F  (*((volatile unsigned long *)0x40025404))

/*GPIO Interrupt Both Edges - GPIOIBE*/
#define GPIOIBE_PORT_F  (*((volatile unsigned long *)0x40025408))

/*
GPIO Interrupt Event - GPIOIEV 0-falling, 1-rising
*/
#define GPIOIEV_PORT_F  (*((volatile unsigned long *)0x4002540C))

/*
GPIO Interrupt Mask - GPIOIM 0-Masked, 1-sent to INT controller
*/
#define GPIOIM_PORT_F  (*((volatile unsigned long *)0x40025410))

/*
GPIO Raw Interrupt Status - GPIORIS
*/
#define GPIORIS_PORT_F  (*((volatile unsigned long *)0x40025414))

/*
GPIO Masked Interrupt Status - GPIOMIS
*/
#define GPIOMIS_PORT_F  (*((volatile unsigned long *)0x40025418))

/*
GPIO Interrupt Clear - GPIOICR
*/
#define GPIOICR_PORT_F  (*((volatile unsigned long *)0x4002541C))


/*
Interrupt 0-31 Set Enable (EN1), offset 0x104
*/
#define INT_0_31_ENABLE  (*((volatile unsigned long *)0xE000E100))

/*
Interrupt 32-63 Set Enable (EN1), offset 0x104
*/
#define INT_32_63_ENABLE  (*((volatile unsigned long *)0xE000E104))


void Systick_Handler(void);
void Gpio_PortFIRQ(void);

#endif
