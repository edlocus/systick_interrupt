#include "TM4C123GH6PM.h"

#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80

#define SYS_CLOCK_HZ 16000000U

#define RED_LED     BIT1
#define BLUE_LED    BIT2
#define GREEN_LED   BIT3

/**
 * main.c
 */
int main(void)
{
    SYSCTL->RCGC2 |= (1<<5);  // clocking gating to enable port F

    /*** Set PF1 - PF3 direction as output ***/
    GPIOF->DIR |= 0x0E;  // Select selected gpio direction as output
    GPIOF->DEN |= 0x0E;  // enable digital function

    /*** init systick timer ***/
    SysTick->LOAD = SYS_CLOCK_HZ / 4U; // 250ms count
    SysTick->VAL = 0;
    SysTick->CTRL |= (BIT0) + (BIT2); /* BIT0 - Enable systick timer, BIT1 - Interrupt mode, BIT2 - System clock 16MHZ*/

    while(1)
    {
        while(SysTick->CTRL & (1<<16))
        {
            SysTick->VAL = 0;
            GPIOF->DATA ^= RED_LED;
        }
    }

}


